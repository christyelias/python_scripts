class prime_number:
    """
    Python program to check if the input number is prime or not
    """
    def __init__(self, input_number):
        self.input_number = input_number

    """ prime numbers are greater than 1 """
    def check_prime(self):
        if self.input_number > 1:
            """ check for factors """
            for i in range(2, self.input_number):
                if (self.input_number % i) == 0:
                    return False
                else:
                    return True
        
            """ if input number is less than
            or equal to 1, it is not prime """

        else:
            # print(self.input_number,"is not a prime number")
            return False

def main():
    # print("Begin")
    prime_obj = prime_number(5)
    prime_result = prime_obj.check_prime()
    print(prime_result)

if __name__ == "__main__":
    main()    
