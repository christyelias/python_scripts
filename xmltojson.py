import json
import xmltodict
import os
from os.path import basename

class XmlJsonParser:
#     Class to parse the xml/json file and convert them to either format
    def __init__(self, input_file):
        self.infile = input_file
#     Function to convert xml file to json file
    def xmltojson(self):
    	# xmlfile = self.infile
    	# setup jsonfile name
        outfile_name = str(os.path.splitext(basename(self.infile))[0])+".json"
        outpath = str(os.path.dirname(os.path.abspath(self.infile)))+"/"
        jsonfile = outpath+outfile_name

        with open(self.infile, 'r') as xml:

            try:
                xmlString = xml.read()
                #print("XML input (sample.xml):")
                #print(xmlString)
                jsonString = json.dumps(xmltodict.parse(xmlString), indent=4)
                #print("\nJSON output(output.json):")
                #print(jsonString)

                with open("jsonfile", 'w') as f:
                    f.write(jsonString)
                    print('Your file has been parsed.\n\n')

            except:
                print("Unexpected error opening %s" % (xmlfile))
        
        # if json text has to be returned uncomment the line below and comment the ones below
        # return jsonString 