import csv
import sys
import os
import re
import json
from bs4 import BeautifulSoup
import requests
from elasticsearch import Elasticsearch

currentDirectory = os.path.dirname(os.path.realpath(__file__))
dataFilePath = currentDirectory + '\\Data\\'

class ElasticSearchImporter(object):
    def importToDb(self, fileName, indexDbName, indexType="default"):
		csv.field_size_limit(sys.maxsize)
		es = Elasticsearch()
		list = []
		headers = []
		index = 0

		f = open(dataFilePath+fileName, 'rb')
		reader = csv.reader(f)

		try:
			for row in reader:
				try:
					if(index == 0):
						headers = row
					else:
						obj = {}
						for i, val in enumerate(row):
							obj[headers[i]] = val
						# put document into elastic search
						# Elastic Search 6.0 + doesn't support multiple types in one index.
						es.index(index=indexDbName, doc_type=indexType, body=obj)

				except Exception as e:
					print(index)
					print(e)
			
				index = index + 1
		except:
			print ('error')
		
		if not f.closed:
			f.close()

importer = ElasticSearchImporter()
importer.importToDb("result.csv", "testIndex", indexType="default")