import json
import os
import sys
import csv
import elasticsearch
from elasticsearch import helpers
from elasticsearch import Elasticsearch

# make sure no encode issue
#reload(sys)
#sys.setdefaultencoding('utf8')

currentDirectory = os.path.dirname(os.path.realpath(__file__))
dataFilePath = currentDirectory + '/export/'

# export elasticsearch into csv file
class ElasticSearchExporter(object):
    def export(self, outputFiles, host, indexDbName, docType, queryString, queryJson=None, size = 100, fields = "all", delimiter =","):
        es = Elasticsearch(host)

        # define query
        query = None
        if queryJson:
            query = json.loads(queryJson)
        else:
            query = dict(
                query = dict(
                    query_string=dict(
                        query=queryString
                    )
                )
            )
            #query["sort"] = [{"time":"desc"}]
            query["size"] = size

        #Get real index name in case index is alias
        aliases = es.indices.get_alias()
        for i in aliases:
            if indexDbName in aliases[i]['aliases']:
                indexDbName = i

        #Fetch the mapping in order to create the header
        mapping=es.indices.get_mapping(index=indexDbName,doc_type=docType)[indexDbName]['mappings'][docType]['properties'].keys()

        #Set handler to elasticsearch
        scanResponse = helpers.scan(client=es, query=query, scroll= "10m", index=indexDbName,size=size, doc_type=docType, clear_scroll=False, request_timeout=3000)


        # write to file
        counter = 0
        with open(outputFiles, 'w') as f:
            if fields == "all":
                w = csv.DictWriter(f, mapping, delimiter=delimiter,quoting=csv.QUOTE_MINIMAL)
            else:
                fields = fields.split(",")
                w = csv.DictWriter(f, [i for i in mapping if i in fields], delimiter=delimiter, extrasaction='ignore',quoting=csv.QUOTE_MINIMAL )
            w.writeheader()
            try:
                for row in scanResponse:
                    for key,value in row['_source'].iteritems():
                        row['_source'][key] = unicode(value)
                    _ = w.writerow(row['_source'])
                    counter +=1
            except elasticsearch.exceptions.NotFoundError:
                pass
            except elasticsearch.exceptions.RequestError:
                pass
            
            
        print('%s lines was expotred to file: %s'%(counter,outputFiles))



exporter = ElasticSearchExporter()
exporter.export(dataFilePath + "results2.csv",
                "localhost",
                "performtracking",
                "default",
                "loadingTime:[10000 TO 100000] AND referrer:logon",
                None,
                100,
                "all",
                ",")

