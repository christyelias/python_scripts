"""
An example in class based decorators in python.
"""


class Decorator(object):
    '''
    '''
    func = None

    def __init__(self, func, *args, **kwargs):
        print('init', func, args, kwargs)
        self.func = func

    def __call__(self, *args, **kwargs):
        print('call', args, kwargs)
        assert self.func is not None
        return None


@Decorator
def foo():
    pass

if __name__ == '__main__':
    foo()