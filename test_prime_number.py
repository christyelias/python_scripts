import prime_number
import unittest

class test_check_prime(unittest.TestCase):
    """
    Test the check_prime function from the prime_number library
    """

    def test_check_prime_number_false(self):
        prime_obj = prime_number.prime_number(4)
        result = prime_obj.check_prime()
        self.assertEqual(result, False)
    
    def test_check_prime_number_true(self):
        prime_obj = prime_number.prime_number(5)
        result = prime_obj.check_prime()
        self.assertEqual(result, True)

if __name__ == '__main__':
    unittest.main()
