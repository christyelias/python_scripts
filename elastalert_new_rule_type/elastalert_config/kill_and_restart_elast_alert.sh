#!/bin/bash
#0 */12 * * * /etc/curator/kill_and_restart_elast_alert.sh >> /etc/curator/kill_and_restart_elast_alert.log 2>&1 & echo $! > /etc/curator/kill_and_restart_elast_alert.pid
# Re run every 12 hrs

# Kill FVC
FVC_APP_ID=$(ps -ef | grep -i fvc_config | grep -i yaml | awk '{print $2}')
if [ -n "${FVC_APP_ID}" ]; then
    echo -e "$(date) : Stopping and restarting the FVC Elast alert"
        kill -9 $FVC_APP_ID
else
        echo -e "$(date) : FVC Elast alert is not running , restarting"
fi


# Kill HLB 10 mins rule
HLB_APP_ID_10=$(ps -ef | grep -i hlb_config_10_mins | grep -i yaml | awk '{print $2}')
if [ -n "${HLB_APP_ID_10}" ]; then
    echo -e "$(date) : Stopping and restarting the HLB Elast alert 10 mins instance "
        kill -9 $HLB_APP_ID_10
else
        echo -e "$(date) : HLB Elast alert 10 mins is not running , restarting"
fi

# Kill hlb_config_4_hrs rule
HLB_APP_ID_4=$(ps -ef | grep -i hlb_config_4_hrs | grep -i yaml | awk '{print $2}')
if [ -n "${HLB_APP_ID_4}" ]; then
    echo -e "$(date) : Stopping and restarting the HLB Elast alert 4 Hrs instance"
        kill -9 $HLB_APP_ID_4
else
        echo -e "$(date) : HLB Elast alert 4 hrs is not running , restarting"
fi


# Kill NILB 10 mins rule
NILB_APP_ID_10=$(ps -ef | grep -i nilb_config_10_mins | grep -i yaml | awk '{print $2}')
if [ -n "${NILB_APP_ID_10}" ]; then
    echo -e "$(date) : Stopping and restarting the NILB Elast alert 10 mins instance "
        kill -9 $NILB_APP_ID_10
else
        echo -e "$(date) : NILB Elast alert 10 mins is not running , restarting"
fi

# Kill nilb_config_4_hrs rule
NILB_APP_ID_4=$(ps -ef | grep -i nilb_config_4_hrs | grep -i yaml | awk '{print $2}')
if [ -n "${NILB_APP_ID_4}" ]; then
    echo -e "$(date) : Stopping and restarting the NILB Elast alert 4 Hrs instance"
        kill -9 $NILB_APP_ID_4
else
        echo -e "$(date) : NILB Elast alert 4 hrs is not running , restarting"
fi



#Start all 3 elastalert
/usr/bin/python2 /usr/bin/elastalert --config /home/centos/elastalert/nilb_config_10_mins.yaml --verbose --start NOW >> /etc/curator/nilb_10_mins_elastalert.log 2>&1 & echo $! > /etc/curator/nilb_10_mins_elastalert.pid

/usr/bin/python2 /usr/bin/elastalert --config /home/centos/elastalert/nilb_config_4_hrs.yaml --verbose --start NOW >> /etc/curator/nilb_4_hrs_elastalert.log 2>&1 & echo $! > /etc/curator/nilb_4_hrs_elastalert.pid


/usr/bin/python2 /usr/bin/elastalert --config /home/centos/elastalert/hlb_config_10_mins.yaml --verbose --start NOW >> /etc/curator/hlb_10_mins_elastalert.log 2>&1 & echo $! > /etc/curator/hlb_10_mins_elastalert.pid

/usr/bin/python2 /usr/bin/elastalert --config /home/centos/elastalert/hlb_config_4_hrs.yaml --verbose --start NOW >> /etc/curator/hlb_4_hrs_elastalert.log 2>&1 & echo $! > /etc/curator/hlb_4_hrs_elastalert.pid

/usr/bin/python2 /usr/bin/elastalert --config /home/centos/elastalert/fvc_config.yaml --verbose --start NOW >> /etc/curator/fvc_elastalert.log 2>&1 & echo $! > /etc/curator/fvc_elastalert.pid

/usr/bin/python2 /usr/bin/elastalert --config /home/centos/elastalert/hlb_new.yaml --verbose --start 2018-10-10 >> /etc/curator/elast_alert.log 2>&1 & echo $! > /etc/curator/elast_alert.pid



