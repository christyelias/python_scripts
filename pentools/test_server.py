#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket
import os
from pathlib import Path

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 12454)) #if the clients/server are on different network you shall bind to ('', port)

s.listen(12455)
c, addr = s.accept()
print('{} connected.'.format(addr))

data_folder = os.getcwd()
file_to_open = data_folder / "benign.bat"
print(file_to_open)
f = open(file_to_open, "rb")
l = os.path.getsize("file_to_open")
m = f.read(l)
c.send_all(m)
f.close()
print("Done sending...")


# from pathlib import Path
# data_folder = Path("C:/Users/Christy/Documents/test/")
# file_to_open = data_folder / "benign.bat"
# print(file_to_open.read_text())