# Import Elasticsearch package 
from elasticsearch import Elasticsearch as es
# Connect to the elastic cluster
# es=Elasticsearch([{'host':'localhost','port':9200}])

class QueryValidation:
    
    def __init__(self, es_server, qindex, qdoc_type, qbody, qexplain):
    # def __init__(self, qindex, qdoc_type, qbody, qexplain):
        self.es_server = es_server
        self.qindex = qindex
        self.qdoc_type = qdoc_type
        self.qbody = qbody
        self.qexplain = qexplain

    def query_validator(self):
        es = self.es_server
        res = es.indices.validate_query(index= self.qindex, doc_type=self.qdoc_type, body=self.qbody, explain= self.qexplain)
        return res

def main():
    es_server = Elasticsearch([{'host':'localhost','port':9200}])
    rule1 = QueryValidation(es_server, "'megacorp'", "'employee'", """"{'query':{'match_all':{}}}""", "true")
    # rule1 = QueryValidation("'megacorp'", "'employee'", """"{'query':{'match_all':{}}}""", "true")
    validity = rule1.query_validator()
    print(validity)

if __name__ == "__main__":
    main()






