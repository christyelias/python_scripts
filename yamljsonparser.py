import os
from os.path import basename
import json
from collections.abc import Mapping, Sequence
from collections import OrderedDict
from ruamel import yaml as yaml1
from ruamel.yaml.error import YAMLError
from ruamel.yaml.comments import CommentedMap
from ruamel.yaml.scalarstring import PreservedScalarString, SingleQuotedScalarString
from ruamel.yaml.compat import string_types, MutableMapping, MutableSequence

yaml = yaml1.YAML()

class OrderlyEncoder(json.JSONEncoder):
#     Class to Keep the order in the json file intact  
    def default(self, o):
        if isinstance(o, Mapping):
            return OrderedDict(o)
        elif isinstance(o, Sequence):
            return list(o)
        return json.JSONEncoder.default(self, o)

class YamlJsonParser:    
#     Class to parse the json/yaml file and convert them to either format
    def __init__(self, input_file):
        self.infile = input_file
     
    def preserve_literal(self, s):
        return PreservedScalarString(s.replace('\r\n', '\n').replace('\r', '\n'))

    def walk_tree(self, base):
        if isinstance(base, MutableMapping):
            for k in base:
                v = base[k]  # type: Text
                if isinstance(v, string_types):
                    if '\n' in v:
                        base[k] = self.preserve_literal(v)
                    elif '${' in v or ':' in v:
                        base[k] = SingleQuotedScalarString(v)
                else:
                    self.walk_tree(v)
        elif isinstance(base, MutableSequence):
            for idx, elem in enumerate(base):
                if isinstance(elem, string_types):
                    if '\n' in elem:
                        base[idx] = self.preserve_literal(elem)
                    elif '${' in elem or ':' in elem:
                        base[idx] = SingleQuotedScalarString(elem)
                else:
                    self.walk_tree(elem)    
                    
    def parseyaml(self):
#         def parseyaml(intye, outtype):
#         infile = input('Please enter a {} filename to parse: '.format(intype))
#         outfile = input('Please enter a {} filename to output: '.format(outtype))
        infile = self.infile
        # print(infile)
        outfile_name = str(os.path.splitext(basename(infile))[0])+".json"
        # print(outfile_name)
        outpath = str(os.path.dirname(os.path.abspath(infile)))+"/"
        # print(outpath)
        outfile = outpath+outfile_name
        # print(outfile)
        with open(infile, 'r') as stream:
            # print(stream)
            try:
                # print(stream)
                datamap = yaml.load(stream)
                # print(datamap)
                with open(outfile, 'w') as output:
                    # print(outfile)
                    output.write(OrderlyEncoder(indent=2).encode(datamap))
                    # return True
            except YAMLError as exc:
                # print("in except")
                print(exc)
                return False
        print('Your file has been parsed.\n\n')
        
    def parsejson(self):
#         infile = input('Please enter a {} filename to parse: '.format(intype))
#         outfile = input('Please enter a {} filename to output: '.format(outtype))
        infile = self.infile
        print(infile)
        outfile_name = str(os.path.splitext(basename(infile))[0])+".yaml"
        print(outfile_name)
        outpath = str(os.path.dirname(os.path.abspath(infile)))+"/"
        print(outpath)
        outfile = outpath+outfile_name
        print(outfile)
        
        with open(infile, 'r') as stream:
            try:
                datamap = json.load(stream, object_pairs_hook=CommentedMap)
                self.walk_tree(datamap)
                with open(outfile, 'w') as output:
                    yaml.dump(datamap, output)
            except YAMLError as exc:
                print(exc)
                return False
        print('Your file has been parsed.\n\n')

# def main():
# 	print("***********************************************************")
# 	query1 = YamlJsonParser('/home/christy/Documents/test/elastalert-rule1.yaml')
# 	# print(query1.infile)
# 	x=query1.parseyaml()
# 	print("***********************************************************")
# 	print(x)

# 	print("***********************************************************")
# 	query1 = YamlJsonParser('/home/christy/Documents/test/elastalert-rule2.json')
# 	# print(query1.infile)
# 	x=query1.parsejson()
# 	print("***********************************************************")
# 	print(x)

# if __name__ == "__main__":
# 	main()